from ...abc import HylleraasQMInterface, Parser, Runner
from ...presets import SETTINGS_LEGACY as SETTINGS
from hyset import ComputeSettings, create_compute_settings
from hyobj import Molecule
import warnings
from ....utils import unique_filename
from .template import GAMMA_SINGLE

from typing import Any, Optional, Tuple, Union
from pathlib import Path

import numpy as np
import os

from .output_parser import OutputParser


class VASP(HylleraasQMInterface):
    """VASP interface"""

    def __init__(self,
                 method: dict,
                 compute_settings: Optional[ComputeSettings] = None):
        """
        This constructor prepares the VASP interface for running several
        molecules using the same VASP input script and kpoints settings.

        Method need to contain.
        method['incar_file'] and/or method['incar_options'];
            method['incar_options'] overrides options in method['incar_file']
        method['potential_specification']
            ['species_mapping']: dict, maps elements to the actual potential,
                e.g. {"O" : "O_h"} for using a hard oxygen potential.
                In the absence of mappings, the potential will default to
                that with the same name as the element.
            ['path']: path to VASP potential folder
        method['kpoints'] None or dict
            ['file'] path
            ['string'] file contents
        """
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.executable: str
        self.launcher: Union[list, str]
        self.executable, self.launcher = \
            self._set_executable(method, self.compute_settings)

        self.incar_string = self._get_incar_string(method)
        self.kpoints_string = self._get_kpoints_string(method)

        self.potential_specification = method.get('potential_specification')

        self.runner: Runner = getattr(
            self.compute_settings,
            str(SETTINGS[self.compute_settings.name]['runner']),
        )

        self.parser: Parser = OutputParser

        # to_check = method.get('check_version', True)
        # self.version: pv.Version = self.check_version() if to_check else None

    def check_version(self) -> str:
        """Check the version"""
        pass

    def author(self):
        """Return who wrote this interface"""
        return 'henriasv@uio.no'

    def get_energy(self,
                   molecule,
                   unit_cell: Optional[Union[list, np.array]] = None,
                   pbc: Optional[list] = None):
        """Compute energy"""
        result = self.run(molecule, unit_cell, pbc)
        return result['energy']

    def get_gradient(self,
                     molecule,
                     unit_cell: Optional[Union[list, np.array]] = None,
                     pbc: Optional[list] = None):
        """Compute gradient"""
        result = self.run(molecule, unit_cell, pbc)
        return result['gradient']

    def get_hessian(self, molecule: Any) -> np.array:
        """Compute Hessian."""
        pass

    def _set_executable(
            self, method: dict,
            compute_settings: ComputeSettings) -> Tuple[str, Union[list, str]]:
        """Set executable for serial/parallel compuation with VASP.

        Parameter
        ---------
        method: dict
            method input

        compute_settings: :obj:`hyset.ComputeSettings`
            compute_Settings

        Returns
        -------
        str
            name of binary

        """
        executable: str = method.get('executable', 'vasp_cpu')
        num_procs = compute_settings.mpi_procs
        if num_procs is None:
            num_procs = 1
        launcher: Union[list, str]
        launcher = f'mpirun -n {num_procs}'
        return executable, launcher

    def _get_incar_string(self, method: dict) -> str:
        """Get template for VASP from method.
            For VASP, we need to put each simulation in a folder in the working
            directory. This folder can have a name that is the hash of all its
            contents: KPOINTS, INCAR, POSCAR, POTCAR

            method['incar_options'] overrides options in method['incar_file']
        """
        incar_template = method.get("incar_file")

        incar_option_dict = {"SYSTEM": "hylleraas_al"}

        try:
            path = self.abs_path(incar_template)
            with open(path, "r") as ifile:
                contents = ifile.read()
                lines = contents.replace(";", "\n").split("\n")
                for line in lines:
                    split = line.replace(" ", "").split("=")
                    if len(split) == 2:
                        key, value = split
                        incar_option_dict.update({key: value})
                    elif len(split) == 1:
                        if len(split[0]) == 0:
                            pass
                        else:
                            raise ValueError("There is a line in your INCAR \
                                             that is not a key value line")
                    else:
                        raise ValueError("Something wrong with key value pairs\
                                          in INCAR")

        except (OSError, TypeError):
            pass

        incar_options = method.get("incar_options")
        if incar_options is not None:
            incar_option_dict.update(incar_options)

        return "\n".join([f"{key} = {value}" for key, value in
                          incar_option_dict.items()])

    def _get_kpoints_string(self, method: dict) -> str:
        kpoints_template = GAMMA_SINGLE
        kpoints_options = method.get("kpoints")

        if kpoints_options is None or kpoints_options == {}:
            return kpoints_template
        else:
            if isinstance(kpoints_options, dict):
                kpoints_file = kpoints_options.get('file')
                kpoints_string = kpoints_options.get('string')
                if kpoints_string is not None:
                    if kpoints_file is not None:
                        raise ValueError("Cannot provide both kpoints file \
                                         and string")
                    else:
                        return kpoints_string
                else:
                    with open(self.abs_path(kpoints_file), "r") as ifile:
                        return ifile.read()
            else:
                raise ValueError("method['kpoints'] needs to be dict")

    def abs_path(self, filename: Union[str, Path]) -> Path:
        """Get absolute path of filename in workdir.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            (relative) path to file

        Returns
        -------
        :obj:`pathlib.Path`
            absolute path for file (assumed to be inside work_dir)

        """
        return Path(self.compute_settings.work_dir) / Path(filename)

    def run(
        self,
        molecule: Molecule,
        unit_cell: Optional[Union[list, np.array]] = None,
        pbc: Optional[list] = None,
    ) -> dict:
        """Run a VASP calculation.

        Parameter
        ---------
        molecule: :obj:`hyobj.Molecule`
            Molecule
        unit_cell : list or np.ndarray, optional
            unit_cell, default None
        pbc: list, optional
            periodic boundary conditions, default None


        Returns
        -------
        dict
            parsed results

        """
        unit = molecule.properties.get('unit', 'bohr')

        if 'angs' not in unit.lower():
            raise TypeError(f'unit {unit} not supported in HSP VASP interface')
        if not sum(np.array(pbc) == np.array([True, True, True])) == 3:
            raise TypeError('Only periodic boundary condition supported in \
                            HSP VASP interface')

        # Create input files
        poscar_string, outcar_reorder = self._get_poscar_string(
            molecule=molecule, unit_cell=unit_cell, pbc=pbc)
        potcar_string = self._get_potcar_string(molecule=molecule)

        foldername = self.abs_path(unique_filename([self.incar_string,
                                                    self.kpoints_string,
                                                    poscar_string,
                                                    potcar_string]))

        os.makedirs(foldername, exist_ok=True)
        incar_filename = self.abs_path(os.path.join(foldername, "INCAR"))
        kpoints_filename = self.abs_path(os.path.join(foldername, "KPOINTS"))
        poscar_filename = self.abs_path(os.path.join(foldername, "POSCAR"))
        potcar_filename = self.abs_path(os.path.join(foldername, "POTCAR"))
        outcar_filename = self.abs_path(os.path.join(foldername, "OUTCAR"))

        with open(incar_filename,   'wt') as f:
            f.write(self.incar_string)
        with open(kpoints_filename, 'wt') as f:
            f.write(self.kpoints_string)
        with open(poscar_filename,  'wt') as f:
            f.write(poscar_string)
        with open(potcar_filename,  'wt') as f:
            f.write(potcar_string)

        vasp_ex = self.compute_settings.find_bin_in_env(self.executable)

        running_dict = {
            'program': vasp_ex,
            'launcher': self.launcher,
        }

        # To make runner launch vasp inside folder with
        # INCAR, POSCAR, KPOINTS and POTCAR
        self.runner.work_dir = self.abs_path(foldername)
        _ = self.runner.run(running_dict)

        result: dict = self.parser.parse(outcar_filename, outcar_reorder)

        # HAS: Assuming this is important for assessing whether
        # simulations need to be rerun?
        result['input_file'] = str(foldername)
        result['output_file'] = str(os.path.join(foldername, "OUTCAR"))

        if result['errors']:
            warnings.warn('\n'.join(result['errors']))

        return result

    def _get_poscar_string(self, molecule: Molecule,
                           unit_cell: Optional[Union[list, np.array]] = None,
                           pbc: Optional[list] = None,
                           comment: str = "Autogenerated by \
Hylleraas Software Platform VASP interface"):
        """Create VASP POSCAR from Hylleraas Molecule and unit cell."""

        poscar_content = comment + "\n"
        poscar_content += "1.0\n"

        if pbc is not None and unit_cell is not None:
            poscar_content += " ".join([f"{e:.16f}" for e in unit_cell[0]]) \
                + "\n"
            poscar_content += " ".join([f"{e:.16f}" for e in unit_cell[1]]) \
                + "\n"
            poscar_content += " ".join([f"{e:.16f}" for e in unit_cell[2]]) \
                + "\n"

        numbers = np.array(molecule.atomic_numbers)
        positions = np.array(molecule.coordinates)
        names = np.array(molecule.atoms)

        # Need to supply particle types one by one in poscar.
        # Sorting so that we start with the lowest atomic number
        order = np.argsort(numbers)

        _, unique_counts = np.unique(numbers[order], return_counts=True)
        unique_names = np.unique(names[order])

        poscar_content += " ".join(unique_names) + "\n"
        poscar_content += " ".join(map(str, unique_counts)) + "\n"

        poscar_content += "Cartesian\n"

        # Inserting particles in the same order as the sorted unique species
        # names
        for i in order:
            poscar_content += " ".join([f"{positions[i,j]}"
                                        for j in [0, 1, 2]]) + "\n"

        return poscar_content, np.argsort(order)

    def _get_potcar_string(self, molecule: Molecule):
        mappings = {}
        mappings.update(self.potential_specification['species_mapping'])
        print(self.potential_specification["species_mapping"])
        print(mappings)
        order = np.argsort(molecule.atomic_numbers)
        species_in_order = np.unique(np.array(molecule.elements)[order])

        for i, species in enumerate(species_in_order):
            if species in mappings.keys():
                species_in_order[i] = mappings[species]

        return "".join([open(os.path.join(self.potential_specification['path'],
                                          folder, "POTCAR")).read() for folder
                        in species_in_order])
