from ase.io.vasp import read_vasp_out
import numpy as np
from ...abc import Parser

ERROR_EXCEPTIONS: list = []
WARNING_EXCEPTIONS: list = []


class OutputParser(Parser):
    """VASP output parser"""
    @classmethod
    def parse(cls, outpath, outcar_reorder=None):
        result: dict = {}
        errors: dict = {}
        warnings: dict = {}

        frame = read_vasp_out(outpath)
        result['gradient'] = np.array(frame.get_forces())
        result['energy'] = frame.get_potential_energy()
        result['errors'] = errors
        result['warnings'] = warnings

        if outcar_reorder is not None: 
            result['gradient'][:, :] = result['gradient'][outcar_reorder, :]

        return result
        

"""
OutputParser(Parser):

    @classmethod
    def parse(cls, outpath) -> dict:
        result: dict = {}
        errors: list = []
        warnings: list = []

        with open(outpath, 'rt') as output_file:
            lines = output_file.readlines()

        # result['full_output'] = '\n'.join(lines)

        for i, line in enumerate(lines):
            if 'Total number of' in line:
                if '- Atoms:' in lines[i + 1]:
                    num_atoms = int(lines[i + 1].split()[-1])
            if 'ENERGY|' in line:
                en = float(line.split()[-1])
                result['energy'] = en

            if 'ATOMIC FORCES in [a.u.]' in line:
                istart = i + 3
                gradient = []
                for j in range(istart, istart + num_atoms):
                    line_split = lines[j].split()
                    xyz = [float(line_split[k]) for k in range(3, 6)]
                    gradient.append(xyz)

                result['gradient'] = np.array(gradient)

            if 'CP2K| version string:' in line:
                result['version'] = line.split()[-1]

            if 'GLOBAL| Total number of message passing processes' in line:
                result['mpi_procs'] = line.split()[-1]

            if 'GLOBAL| Number of threads for this process' in line:
                result['smp_threads'] = line.split()[-1]

            if 'error' in line.lower():
                if not any([ex in line for ex in ERROR_EXCEPTIONS]):
                    errors.append(line)

            if 'warning' in line.lower():
                warning = line
                if not any([ex in warning for ex in WARNING_EXCEPTIONS]):
                    warnings.append(warning)

            if 'abort' in line.lower():
                if not any([ex in line for ex in ERROR_EXCEPTIONS]):
                    errors.append(line)

        result['errors'] = errors
        result['warnings'] = warnings

        result['output_file'] = str(outpath)
        return result
"""