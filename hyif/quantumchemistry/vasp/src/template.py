TEMPLATE = """SYSTEM = vaspsim
ISTART = 0
ISYM = 0
ALGO = All
EDIFF = 1.0E-04
LWAVE = .FALSE.
LCHARG = .FALSE.
LREAL = .FALSE
NELM = 80
ISIF=2; IBRION = -1
NWRITE = 2
ISMEAR = 0
SIGMA = 0.2
ISPIN = 1
IVDW = 0
"""
GAMMA_SINGLE = """Regular 1 x 1 x 1 mesh centered at Gamma
0
Gamma
1 1 1"""
