from dataclasses import dataclass

import numpy as np


@dataclass
class Molecule:
    """Interface to hylleraas Molecule class."""

    atoms: list
    coordinates: np.array
    properties: dict
