import os
import shutil
from typing import Optional

from hyset import ComputeSettings, create_compute_settings
from qcelemental import PhysicalConstantsContext

# from ..importme import HylleraasInterface
from ...utils import get_val_from_arglist, key_in_arglist
from .parser import LondonInput, LondonOutput

constants = PhysicalConstantsContext('CODATA2018')


class London:
    """London interface."""

    def __init__(self, method,
                 compute_settings: Optional[ComputeSettings] = None):
        self.method = method
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        if key_in_arglist(self.method, look_for='executable'):
            self.executable = get_val_from_arglist(self.method,
                                                   look_for='executable')
        else:
            self.executable = 'london.x'

        if key_in_arglist(self.method, look_for='filename'):
            self.filename = get_val_from_arglist(self.method,
                                                 look_for='filename')
        else:
            self.filename = 'mylondoncalc'

        self.filename_inp = self.filename + str('.inp')
        self.filename_out = self.filename + str('.out')

    @classmethod
    def get_input_method(cls, *args) -> dict:
        """Get method input."""
        method: dict = {}
        for arg in args:
            if isinstance(arg, str):
                if os.path.isfile(arg):
                    method = LondonInput.london2json(arg)

        return method

    @classmethod
    def get_input_molecule(cls, *args) -> dict:
        """Get molecule input."""
        molecule: dict = {}
        for arg in args:
            if isinstance(arg, str):
                if os.path.isfile(arg):
                    molecule = LondonInput.london2mol(arg)

        return molecule

    def get_energy(self, molecule):
        """Compute energy with LONDON.

        Returns
        -------
        :obj:`float`
            energy

        """
        if shutil.which(self.executable) is None:
            raise Exception(f'{self.executable} not found in PATH')
        write_file = open(self.filename_inp, 'w')
        input_str = LondonInput.gen_london_input(self.method, molecule)
        write_file.write(input_str)
        write_file.close()
        # os.system(f'{self.executable}
        # {self.filename_inp} > {self.filename_out}')
        energy = LondonOutput.london_output_parser_energy(self.filename_out)
        return energy

    def get_gradient(self, molecule):
        """Parse gradient from Energy computation with LONDON.

        Returns
        -------
        :obj:`array`
            gradient

        """
        if not os.path.isfile(self.filename_out):
            if shutil.which(self.executable) is None:
                raise Exception(f'{self.executable} not found in PATH')
            write_file = open(self.filename_inp, 'w')
            input_str = LondonInput.gen_london_input(self.method, molecule)
            write_file.write(input_str)
            write_file.close()
            # os.system(f'{self.executable}
            # {self.filename_inp} > {self.filename_out}')

        gradient = LondonOutput.london_output_parser_gradient(
            self.filename_out, molecule.num_atoms).ravel()

        return gradient

    @property
    def version(self):
        """Set interface version."""
        return '0.0'

    @property
    def author(self):
        """Set author's name or email adress."""
        return 'tilmann.bodenstein@kjemi.uio.no'
