from pathlib import Path
from typing import List, Union

import numpy as np

from ....utils import bohr2angstrom, read_from_str
from ...abc import Parser

ERROR_EXCEPTIONS: List[str] = ['DIIS Error']
WARNING_EXCEPTIONS: List[str] = ['Please study these warnings very carefully']


class OrcaOutput(Parser):
    """Orca output class."""

    @classmethod
    def parse(cls, output: Union[str, Path]) -> dict:
        """Parse orca output.

        Parameter
        ---------
        output: str or :obj:`pathlib.Path`
            output str or path to output file

        Returns
        -------
        dict
            parsed results

        """
        lines = read_from_str(output)

        result: dict = {}
        errors: list = []
        warnings: list = []
        geo_conv = False

        while True:
            line: str = ''
            try:
                line = next(lines)
            except StopIteration:
                break

            if 'Program Version' in line:
                result['version'] = line.split()[2]

            if 'FINAL SINGLE POINT ENERGY' in line:
                result['energy'] = float(line.split()[4])

            if 'parallel MPI-processes' in line:
                result['mpi_procs'] = int(line.split()[4])

            # if 'CARTESIAN GRADIENT' in line:
            #     lines.__next__()
            #     lines.__next__()
            #     result['gradient']  = []
            #     while (line.strip()!=''):
            #         line = next(lines)
            #         grad = [float(x) for x  in  line.split()[3:6]]
            #         if grad:
            #             result['gradient'] .append(grad )
            #     result['gradient'] = np.array(result['gradient'] )

            if 'THE OPTIMIZATION HAS CONVERGED':
                geo_conv = True

            if 'Error' in line:
                if not any([ex in line for ex in ERROR_EXCEPTIONS]):
                    errors.append(line)

            if 'warning' in line.lower():
                warning = ' '.join((line, lines.__next__()))
                if not any([ex in warning for ex in WARNING_EXCEPTIONS]):
                    warnings.append(warning)

        result['errors'] = errors
        result['warnings'] = warnings

        try:
            output_path = Path(output)
        except (OSError, TypeError):
            pass
        else:
            if Path(output_path.stem + '.hess').exists():
                result['hessian'] = OrcaOutput.extract_hessian(output_path)
                result['frequencies'] = (
                                        OrcaOutput.
                                        extract_vibrational_frequencies(
                                                        output_path)
                                        )
                result['normal_modes'] = (
                                         OrcaOutput.
                                         extract_normal_modes(output_path)
                                         )

            if Path(output_path.stem + '.engrad').exists():
                result['gradient'] = OrcaOutput.extract_gradient(output_path)

            if Path(output_path.stem + '.xyz').exists():
                result['geometry'] = OrcaOutput.extract_geometry(output_path)

                if geo_conv:
                    result['final_geometry_bohr'] = result['geometry']
                    result['final_geometry_angstrom'] = (result['geometry']
                                                         * bohr2angstrom)

        return result

    # @classmethod
    # def energy(cls, output):
    #     """Extract energy from output file."""
    #     with open(output, 'r') as outfile:
    #         lines = outfile.readlines()

    #     energies = []
    #     for i, line in enumerate(lines):
    #         if 'FINAL SINGLE POINT ENERGY' in line:
    #             energies.append(float(line.split()[4]))
    #     if len(energies) > 0:
    #         energy = energies[-1]
    #     else:
    #         warnings.warn(f'could not find energy in file {output}')
    #         energy = None
    #     return energy

    @classmethod
    def extract_gradient(cls, filename: Union[str, Path]) -> np.ndarray:
        """Extract gradient from output file.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            path to output file

        Returns
        -------
        np.ndarray
            gradient

        """
        gradfile = Path(Path(filename).stem + '.engrad')
        if not gradfile.exists():
            raise FileNotFoundError(f'could not find gradient file {gradfile}')

        gradient: list = []

        lines = list(read_from_str(gradfile))
        n_atoms = int(lines[3])
        # energy = float(lines[7])
        for i in (range(11, 11 + 3*n_atoms)):
            gradient.append(float(lines[i]))

        return np.array(gradient).ravel().reshape(-1, 3)

    @classmethod
    def extract_geometry(cls, filename: Union[str, Path]) -> np.ndarray:
        """Extract geometry from output file.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            path to output file

        Returns
        -------
        np.ndarray
            geometry (in bohr)

        """
        xyzfile = Path(Path(filename).stem + '.xyz')
        if not xyzfile.exists():
            raise FileNotFoundError(f'could not find xyz file {xyzfile}')

        xyz: list = []

        lines = read_from_str(xyzfile)
        lines.__next__()  # n_atoms
        lines.__next__()  # comment line

        while True:
            line: str = ''
            try:
                line = next(lines)
            except StopIteration:
                break

            xyz.append([float(x) for x in line.split()[1:4]])

        xyz = np.array(xyz)/bohr2angstrom

        return xyz

    @classmethod
    def extract_hessian(cls, filename: Union[str, Path]) -> np.ndarray:
        """Extract hessian from output file.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            path to output file

        Returns
        -------
        np.ndarray
            hessian

        """
        hessfile = Path(Path(filename).stem + '.hess')
        if not hessfile.exists():
            raise FileNotFoundError(f'could not find hessian file {hessfile}')

        lines = read_from_str(hessfile)
        while True:
            line: str = ''
            try:
                line = next(lines)
            except StopIteration:
                break

            if '$hessian' in line:
                ndim = int(next(lines))
                break

        hessian = np.empty((ndim, ndim))

        for row in range(ndim//5+1):
            if row == ndim//5 and ndim % 5 == 0:
                break
            ii = [int(x) for x in next(lines).split()]
            for i in range(ndim):
                ls = next(lines).split()
                jj = int(ls[0])
                hessian[ii, jj] = np.array([float(x) for x in ls[1:len(ii)+1]])

        return hessian.T

    @classmethod
    def extract_vibrational_frequencies(cls,
                                        filename: Union[str, Path]
                                        ) -> np.ndarray:
        """Extract vibrational frequencies from output file.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            path to output file

        Returns
        -------
        np.ndarray
            vibrational frequencies im cm-1

        """
        hessfile = Path(Path(filename).stem + '.hess')
        if not hessfile.exists():
            raise FileNotFoundError(f'could not find hessian file {hessfile}')

        lines = read_from_str(hessfile)
        while True:
            line: str = ''
            try:
                line = next(lines)
            except StopIteration:
                break

            if '$vibrational_frequencies' in line:
                ndim = int(next(lines))
                break

        freqs = np.empty(ndim)
        for i in range(ndim):
            freqs[i] = float(next(lines).split()[1])

        return freqs

    @classmethod
    def extract_normal_modes(cls, filename: Union[str, Path]) -> np.ndarray:
        """Extract normal_modes from output file.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            path to output file

        Returns
        -------
        np.ndarray
            normal modes

        """
        hessfile = Path(Path(filename).stem + '.hess')
        if not hessfile.exists():
            raise FileNotFoundError(f'could not find hessian file {hessfile}')

        lines = read_from_str(hessfile)
        while True:
            line: str = ''
            try:
                line = next(lines)
            except StopIteration:
                break

            if '$normal_modes' in line:

                ndim1, ndim2 = [int(w) for w in next(lines).split()]
                break

        modes = np.empty((ndim1, ndim2))

        for row in range(ndim1//5+1):
            if row == ndim1//5 and ndim1 % 5 == 0:
                break
            ii = [int(x) for x in next(lines).split()]
            for i in range(ndim2):
                ls = next(lines).split()
                jj = int(ls[0])
                modes[ii, jj] = np.array([float(x) for x in ls[1:len(ii)+1]])

        return modes.T
